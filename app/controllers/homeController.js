const axios = require('axios');
const controller = require('./controller');

class homeController extends controller {

    tokens = new Map();

    async showRepository(req, res) {

        const userId = req.get('userId');
        const gitlabAccessToken = this.tokens.get(userId);
        const projectId = req.params.id;

        if (gitlabAccessToken === undefined) return res.status(400).json({ message: 'Please provide your Gitlab Access Token first!' });

        const response = await this.makeRequest(projectId, gitlabAccessToken);
        res.status(response.status).json(response.data);
    }


    async makeRequest(projectId, gitlabAccessToken) {

        return axios({
            method: 'get',
            url: `https://gitlab.com/api/v4/projects/${projectId}/repository/tree`,
            params: {
                private_token: gitlabAccessToken
            }
        })
            .then(response => {
                return response
            })
            .catch(err => {
                return err.response
            })
    }


    setToken(req, res) {
        this.tokens.set(req.body.userId, req.body.gitlabAccessToken);
        res.status(200).json({ message: 'You can aceess your projects now' });
    }
}

module.exports = new homeController();