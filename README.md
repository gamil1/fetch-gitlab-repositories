# Fetch Gitlab Repositories


This is a simple API that can be used to fetch a list of Gitlab repositories under a project using a Gitlab Access Token.

## Installation

Clone the project to your local repository.

```bash
git clone https://gitlab.com/Sin13/fetch-gitlab-repositories.git
```

Move to the project directory and use npm to install the required modules.

```bash
npm install
```

Start the local server.

```bash
npm start
```

## Usage
1. send a POST request to the API at `localhost:3000/token`, with your id \<userId> and Gitlab access token \<gitlabAccessToken>.

   ![image not found!](https://gitlab.com/Sin13/fetch-gitlab-repositories/-/raw/main/readme-screenshots/post.png)

2. send a GET request to the API at `localhost:3000/projects/:id/repository_tree`, with your id set as a header \<userId> in the request. The id of the project should be provided as part of the request URL.

   ![image not found!](https://gitlab.com/Sin13/fetch-gitlab-repositories/-/raw/main/readme-screenshots/get.png)

## License
[MIT](http://opensource.org/licenses/MIT)
