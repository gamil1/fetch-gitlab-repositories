const express = require('express')
const app = express();

module.exports = class Application {
    constructor() {
        this.setupExpress();
        this.setConfig();
        this.setrouters();
    }

    setupExpress() {
        app.listen(3000, () => console.log('server is listening on port 3000...'));
    }

    setConfig() {
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
    }

    setrouters() {
        app.use(require('app/routes'));
    }
}