const express = require('express');
const router = express.Router();

const homeController = require('app/controllers/homeController');

router.get('/projects/:id/repository_tree', homeController.showRepository);
router.post('/token', homeController.setToken);


module.exports = router;